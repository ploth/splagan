import torch
import numpy as np

from torch.autograd import Variable

class Net(object):
    def xavier_init(self, size):
        in_dim = size[0]
        xavier_stddev = 1. / np.sqrt(in_dim / 2.)
        return Variable(torch.randn(*size) * xavier_stddev, requires_grad=True)

    def reset_grad(self):
        for p in self.params:
            if p.grad is not None:
                data = p.grad.data
                p.grad = Variable(data.new().resize_as_(data).zero_())
