import torch
import torch.nn.functional as nn

from torch.autograd import Variable

from . import Net


class Discriminator(Net):
    def __init__(self, X_dim, h_dim, lr):
        self.Wxh = xavier_init(size=[X_dim, h_dim])
        self.bxh = Variable(torch.zeros(h_dim), requires_grad=True)
        self.Why = xavier_init(size=[h_dim, 1])
        self.bhy = Variable(torch.zeros(1), requires_grad=True)
        self.params = [self.Wxh, self.bxh, self.Why, self.bhy]
        self.lr = lr
        self.solver = optim.Adam(self.params, self.lr)

    def forward(self, X):
        h = nn.relu(X @ self.Wxh + self.bxh.repeat(X.size(0), 1))
        y = nn.sigmoid(h @ self.Why + self.bhy.repeat(h.size(0), 1))
        return y
