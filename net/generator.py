import torch
import torch.nn.functional as nn

from torch.autograd import Variable

from . import Net


class Generator(Net):
    def __init__(self, Z_dim, h_dim, X_dim, lr):
        self.Wzh = xavier_init(size=[Z_dim, h_dim])
        self.bzh = Variable(torch.zeros(h_dim), requires_grad=True)
        self.Whx = xavier_init(size=[h_dim, X_dim])
        self.bhx = Variable(torch.zeros(X_dim), requires_grad=True)
        self.params = [self.Wzh, self.bzh, self.Whx, self.bhx]
        self.lr = lr
        self.solver = optim.Adam(self.params, self.lr)

    def forward(self, z):
        h = nn.relu(z @ Wzh + bzh.repeat(z.size(0), 1))
        X = nn.sigmoid(h @ Whx + bhx.repeat(h.size(0), 1))
        return X
