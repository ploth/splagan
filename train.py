import os
import yaml
import torch
import torch.nn.functional as nn
import torch.autograd as autograd
import torch.optim as optim

from torch.autograd import Variable
from tensorflow.examples.tutorials.mnist import input_data

from net.generator import Generator
from net.discriminator import Discriminator
from net.util.stats import display_stats

with open('config.yaml') as c:
    config = yaml.load(c)

# Load data
mnist = input_data.read_data_sets('../../MNIST_data', one_hot=True)
X_dim = mnist.train.images.shape[1]

# Mini batch size
mb_size = config['mb_size']
# Image size
Z_dim = config['Z_dim']
# Hidden layer size
h_dim = config['h_dim']
c = config['c']
# Learning rate
lr = config['lr']

# Create generator and discriminator
generator = Generator(Z_dim, h_dim, X_dim, lr)
discriminator = Discriminator(X_dim, h_dim, lr)

ones_label = Variable(torch.ones(mb_size, 1))
zeros_label = Variable(torch.zeros(mb_size, 1))

for it in range(100000):
    # Sample data
    z = Variable(torch.randn(mb_size, Z_dim))
    X, _ = mnist.train.next_batch(mb_size)
    X = Variable(torch.from_numpy(X))

    # Dicriminator forward
    g_sample = generator.forward(z)
    d_real = discriminator.forward(X)
    d_fake = discriminator.forward(g_sample)

    # Calculate discriminator loss
    d_loss_real = nn.binary_cross_entropy(d_real, ones_label)
    d_loss_fake = nn.binary_cross_entropy(d_fake, zeros_label)
    d_loss = d_loss_real + d_loss_fake

    # Discriminator backpropagation
    d_loss.backward()

    # Update discriminator
    discriminator.solver.step()

    # Housekeeping - reset gradient
    discriminator.reset_grad()
    generator.reset_grad()

    # Generator forward
    z = Variable(torch.randn(mb_size, Z_dim))
    g_sample = generator.forward(z)
    d_fake = discriminator.forward(g_sample)

    # Calculate generator loss
    g_loss = nn.binary_cross_entropy(d_fake, ones_label)

    # Generator backpropagation
    g_loss.backward()

    # Update generator
    generator.solver.step()

    # Housekeeping - reset gradient
    discriminator.reset_grad()
    generator.reset_grad()

    #  if it % 1000 == 0:
    #      display_stats()
